FROM alpine:3.6 as builder

ENV OVERTURE_VERSION 1.6.1

RUN apk --no-cache add --virtual .build-deps \
        curl \
    && curl -L https://github.com/shawn1m/overture/releases/download/v$OVERTURE_VERSION/overture-linux-amd64.zip >overture.zip \
    && mkdir -p /overture-src \
    && unzip overture.zip -d /overture-src \
    && mkdir -p /overture-dest/bin \
    && cd /overture-src \
    && mv overture-linux-amd64 /overture-dest/bin/overture \
    && chmod +x /overture-dest/bin/overture \
    && mkdir -p /overture-dest/etc \
    && cp * /overture-dest/etc/


FROM scratch

COPY --from=builder /overture-dest/bin /bin

COPY --from=builder /overture-dest/etc /etc

ENTRYPOINT ["/bin/overture"]

CMD ["-c", "/etc/config.json"]
